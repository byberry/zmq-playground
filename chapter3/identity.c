//
// Created by berry on 2020-01-25.
//


//
// 演示请求 - 应答模式所使用的身份。运行此程序本身。
// 请注意，使用函数 s_ 由 zhelpers.h 提供。
//

#include <zmq.h>
#include "zhelpers.h"

int main(void)
{
    void *context = zmq_ctx_new();

    void *sink = zmq_socket(context, ZMQ_ROUTER);
    zmq_bind(sink, "inproc://example");

    // 首先允许 0MQ 设置身份
    void *anonymous = zmq_socket(context, ZMQ_REQ);
    zmq_connect(anonymous, "inproc://example");
    s_send(anonymous, "ROUTER uses a generated UUID");
    s_dump(sink);

    // 然后设置我们自己的身份
    void *identified = zmq_socket(context, ZMQ_REQ);
    zmq_setsockopt(identified, ZMQ_IDENTITY, "PEER2", 5);
    zmq_connect(identified, "inproc://example");
    s_send(identified, "ROUTER socket uses REQ's socket identity");
    s_dump(sink);

    zmq_close(sink);
    zmq_close(anonymous);
    zmq_close(identified);
    zmq_ctx_destroy(context);
    return 0;

}





