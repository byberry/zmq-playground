//
// Created by berry on 2020-01-26.
//


//
// 负载均衡代理
// 演示 CZMQ API 的使用和反应器样式
//

#include <czmq.h>

#define NBR_CLIENTS 10
#define NBR_WORKERS 10
#define WORKER_READY "\001" // 工人已经准备就绪的信号

// 使用 REQ 套接字的基本请求 - 应答客户端
//

static void *
client_task(void *args)
{

    zctx_t *ctx = zctx_new();
    void *client = zsocket_new(ctx, ZMQ_REQ);
    zsocket_connect(client, "ipc://frontend.ipc");

    // 发送请求，获取应答
    while(true){
        zstr_send(client, "HELLO");
        char *reply = zstr_recv(client);
        if (!reply)
            break;
        printf("Client: %s\n", reply);
        free(reply);
        sleep(1);
    }
    zctx_destroy(&ctx);
    return NULL;
}


// 工人使用 REQ 套接字来做负载均衡
//
static void *
worker_task(void *args)
{
    zctx_t *ctx = zctx_new();
    void *worker = zsocket_new(ctx, ZMQ_REQ);
    zsocket_connect(worker, "ipc://backend.ipc");

    // 告诉代理我们已经准备好执行工作
    zframe_t *frame = zframe_new(WORKER_READY, 1);
    zframe_send(&frame, worker, 0);

    // 处理到达的消息
    while(true){
        zmsg_t *msg = zmsg_recv(worker);
        if (!msg)
            break;
        zframe_reset(zmsg_last(msg), "OK", 2);
        zmsg_send(&msg, worker);
    }
    zctx_destroy(&ctx);
    return NULL;
}


// 负载均衡结构，被传递给反应器处理程序
typedef struct {
    void *frontend; // 监听客户端
    void *backend;  // 监听工人
    zlist_t *workers;   // 准备就绪的工人列表
} lbbroker_t;


// 在前台处理来自客户端的输入
int s_handle_frontend(zloop_t *loop, zmq_pollitem_t *poller, void *arg)
{
    lbbroker_t *self = (lbbroker_t *) arg;
    zmsg_t *msg = zmsg_recv(self->frontend);
    if (msg){
        zmsg_wrap(msg, (zframe_t *)zlist_pop(self->workers));
        zmsg_send(&msg, self->backend);

        // 如果我们从 1 个工人变成 0 个工人，则取消在前端的阅读器
        if (zlist_size(self->workers) == 0){
            zmq_pollitem_t poller = {self->frontend, 0 , ZMQ_POLLIN};
            zloop_poller_end(loop, &poller);
        }
    }
    return 0;
}


// 在后端处理来自工人的输入
int s_handle_backend(zloop_t *loop, zmq_pollitem_t *poller, void *arg)
{
    // 使用工人身份以用于负载均衡
    lbbroker_t *self = (lbbroker_t *) arg;
    zmsg_t *msg = zmsg_recv(self->backend);

    if (msg){
        zframe_t *identity = zmsg_unwrap(msg);
        zlist_append(self->workers, identity);

        // 如果我们从 0 个工人变成 1 个工人，就在前端启用阅读器
        if (zlist_size(self->workers) == 1){
            zmq_pollitem_t poller = {self->frontend, 0, ZMQ_POLLIN};
            zloop_poller(loop, &poller, s_handle_frontend, self);
        }
        // 转发消息给客户端，如果他不是一个 READY 消息
        zframe_t *frame = zmsg_first(msg);
        if (memcmp(zframe_data(frame), WORKER_READY, 1) == 0)
            zmsg_destroy(&msg);
        else
            zmsg_send(&msg, self->frontend);
    }
    return 0;

}


int main(void)
{
    zctx_t *ctx = zctx_new();
    lbbroker_t *self = (lbbroker_t *) zmalloc(sizeof(lbbroker_t));
    self->frontend = zsocket_new(ctx, ZMQ_ROUTER);
    self->backend = zsocket_new(ctx, ZMQ_ROUTER);
    zsocket_bind(self->frontend, "ipc://frontend.ipc");
    zsocket_bind(self->backend, "ipc://backend.ipc");

    int client_nbr;
    for (client_nbr = 0; client_nbr < NBR_CLIENTS; client_nbr++)
        zthread_new(client_task, NULL);
    int worker_nbr;
    for (worker_nbr = 0; worker_nbr < NBR_WORKERS; worker_nbr++)
        zthread_new(client_task, NULL);

    // 可用的工人队列
    self->workers = zlist_new();

    // 准备反应器并发射它
    zloop_t *reactor = zloop_new();
    zmq_pollitem_t poller = {self->backend, 0, ZMQ_POLLIN};
    zloop_poller(reactor, &poller, s_handle_backend, self);
    zloop_start(reactor);
    zloop_destroy(&reactor);

    // 当我们完成后，执行适当的清理
    while(zlist_size(self->workers)){
        zframe_t *frame = (zframe_t *) zlist_pop(self->workers);
        zframe_destroy(&frame);
    }
    zlist_destroy(&self->workers);
    zctx_destroy(&ctx);
    free(self);
    return 0;

}














