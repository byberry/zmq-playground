//
// Created by berry on 2020-01-26.
//


//
// 异步客户端到服务器 (DEALER 到 ROUTER)
//
// 在一个进程中运行，为了方便启动
//

#include "czmq.h"
#include "zhelpers.h"



static void *
client_task (void *args)
{
    zctx_t *ctx = zctx_new();
    void *client = zsocket_new(ctx, ZMQ_ROUTER);

    // 设置随机的身份以便跟踪
    char identity[10];
    sprintf(identity, "%04X-%04X", randof(0x10000), randof(0x10000));
    zsocket_set_identity(client, identity);
    zsocket_connect(client, "tcp://localhost:5570");

    zmq_pollitem_t items [] = {{client, 0, ZMQ_POLLIN, 0}};
    int request_nbr = 0;
    while (true){
        // 每秒钟滴答一次，提取到达的消息
        int centitick;
        for (centitick = 0; centitick < 100; centitick++){
            zmq_poll(items, 1, 10*ZMQ_POLL_MSEC);
            if (items[0].revents & ZMQ_POLLIN){
                zmsg_t *msg = zmsg_recv(client);
                zframe_print(zmsg_last(msg), identity);
                zmsg_destroy(&msg);
            }
        }
        zstr_sendf(client, "request #%d", ++request_nbr);
    }
    zctx_destroy(&ctx);
    return ZMQ_NULL;
}


static void server_worker(void *args, zctx_t *ctx, void *pipe);

void *server_task(void *args)
{
    zctx_t *ctx = zctx_new();

    // 前端套接字通过 TCP 与客户端交流
    void *frontend = zsocket_new(ctx, ZMQ_ROUTER);
    zsocket_bind(frontend, "tcp://*:5570");

    // 后端套接字通过 inproc 与工人交流
    void *backend = zsocket_new(ctx, ZMQ_DEALER);
    zsocket_bind(backend, "inproc://backend");

    // 启动工人线程池，准确数字是不重要的
    int thread_nbr;
    for (thread_nbr = 0; thread_nbr < 5; thread_nbr++)
        zthread_fork(ctx, server_worker, NULL);

    // 后端通过一台代理服务器连接到前端
    zmq_proxy(frontend, backend, NULL);

    zctx_destroy(&ctx);
    return NULL;
}

static void
server_worker (void *args, zctx_t *ctx, void *pipe)
{
    void *worker = zsocket_new(ctx, ZMQ_DEALER);
    zsocket_connect(worker, "inproc://backend");

    while(true){
        // DEALER 套接字给我们应答封包和消息
        zmsg_t *msg = zmsg_recv(worker);
        zframe_t *identity = zmsg_pop(msg);
        zframe_t *content = zmsg_pop(msg);
        assert (content);
        zmsg_destroy(&msg);

        // 发回 0..4 应答
        int reply, replies = randof(5);
        for (reply = 0; reply < replies; reply++){
            // 休眠一秒钟的一部分
            zclock_sleep(randof(1000)+1);
            zframe_send(&identity, worker, ZFRAME_REUSE + ZFRAME_MORE);
            zframe_send(&content, worker, ZFRAME_REUSE);
        }
        zframe_destroy(&identity);
        zframe_destroy(&content);
    }
}


// 主线程只是启动几个客户端和一个服务器
// 然后等待服务完成


int main (void)
{
    zthread_new(client_task, NULL);
    zthread_new(client_task, NULL);
    zthread_new(client_task, NULL);
    zthread_new(server_task, NULL);

    // 运行 5 秒钟
    zclock_sleep(5*1000);
    return 0;

}





