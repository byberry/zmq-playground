//
// Created by berry on 2020-01-25.
//


//
// 负载均衡代理
// 这里显示在进程中的客户端和工人
//


#include <zmq.h>
#include <pthread.h>
#include "zhelpers.h"

#define NBR_CLIENTS 10
#define NBR_WORKERS 3
// 对实现为任何东西的数组的队列执行出队操作
#define DEQUEUE(q) memmove (&(q)[0], &(q)[1], sizeof(q) - sizeof(q[0]))

// 使用 REQ 套接字的基本请求 - 应答客户端
// 由于 s_send 和 s_recv 不能处理 0MQ 二进制身份。
// 我们设置一个可打印的文本身份以供路由。
//

static void *
client_task(void (*args))
{
    void *context = zmq_ctx_new();
    void *client = zmq_socket(context, ZMQ_REQ);
    s_set_id(client);   // 设置一个可打印的身份
    zmq_connect(client, "ipc://frontend.ipc");

    // 发送请求，获取应答
    s_send(client, "HELLO");
    char *reply = s_recv(client);
    printf("Client: %s\n", reply);
    free(reply);
    zmq_close(client);
    zmq_ctx_destroy(context);
    return NULL;

}

static void *
worker_task (void *args)
{
    void *context = zmq_ctx_new();
    void *worker = zmq_socket(context, ZMQ_REQ);
    s_set_id(worker);   // 设置一个可打印的身份
    zmq_connect(worker, "ipc://backend.ipc");

    // 告诉代理我们已经准备好执行工作
    s_send(worker, "READY");

    while(1){
       // 读取并保存所有帧，直到我们得到一个空帧
       // 虽然在本例中只有一帧，但可以有多帧
       char *identity = s_recv(worker);
       char *empty = s_recv(worker);
       assert( *empty == 0);
       free(empty);

       // 获取请求，发送应答
       char *request = s_recv(worker);
       printf("Worker: %s\n", request);
       free(request);

       s_sendmore(worker, identity);
       s_sendmore(worker, "");
       s_send(worker, "OK");
       free(request);

    }
    zmq_close(worker);
    zmq_ctx_destroy(context);
    return NULL;

}


int main(void)
{
    // 准备上下文和套接字
    void *context = zmq_ctx_new();
    void *frontend = zmq_socket(context, ZMQ_ROUTER);
    void *backend = zmq_socket(context, ZMQ_ROUTER);
    zmq_bind(frontend, "ipc://frontend.ipc");
    zmq_bind(backend, "ipc://backend.ipc");

    int client_nbr;
    for (client_nbr = 0; client_nbr < NBR_CLIENTS; client_nbr++){
        pthread_t client;
        pthread_create(&client, NULL, client_task, NULL);
    }

    int worker_nbr;
    for (worker_nbr = 0; worker_nbr < NBR_WORKERS; worker_nbr++){
        pthread_t worker;
        pthread_create(&worker, NULL, worker_task, NULL);
    }

    int available_workers = 0;
    char *worker_queue [10];

    while(1){
        zmq_pollitem_t items [] = {
                {backend, 0, ZMQ_POLLIN, 0},
                {frontend, 0, ZMQ_POLLIN, 0},
        };
        // 只有在我们有可用工人时才轮训前端
        int rc = zmq_poll(items, available_workers?2:1,-1);
        if (rc == -1)
            break;  // 中断

        // 在后台处理工人活动
        if (items[0].revents & ZMQ_POLLIN){
            // 对工人身份排队以用于负载均衡
            char *worker_id = s_recv(backend);
            assert(available_workers < NBR_WORKERS);
            worker_queue[available_workers++] = worker_id;

            // 第 2 帧是空的
            char *empty = s_recv(backend);
            assert(empty[0] == 0);
            free(empty);

            // 第 3 帧是 READY, 或者是一个客户端应答身份
            char *client_id = s_recv(backend);

            // 如果客户端应答了，则把剩余的发回给前端
            if (strcmp(client_id, "READY") == 0){
                empty = s_recv(backend);
                assert(empty[0] == 0);
                free(empty);
                char *reply = s_recv(backend);
                s_sendmore(frontend, client_id);
                s_sendmore(frontend, "");
                s_send(frontend, reply);
                free(reply);
                if (--client_nbr == 0)
                    break;  // 在 N 个消息后退出
            }
            free(client_id);

        }

        if (items[1].revents & ZMQ_POLLIN){
            // 现在获取下一个客户端请求，路由到最后使用的工人
            // 客户端请求是 [身份][空][请求]
            char *client_id = s_recv(frontend);
            char *empty = s_recv(frontend);
            assert(empty[0] == 0);
            free(empty);
            char *request = s_recv(frontend);

            s_sendmore(backend, worker_queue[0]);
            s_sendmore(backend, "");
            s_sendmore(backend, client_id);
            s_sendmore(backend, "");
            s_send(backend, request);

            free(client_id);
            free(request);

            // 将下一个工人身份出队并删除
            free(worker_queue[0]);
            DEQUEUE(worker_queue);
            available_workers--;
        }

    }
    zmq_close(frontend);
    zmq_close(backend);
    zmq_ctx_destroy(context);
    return 0;

}








