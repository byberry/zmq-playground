//
// Created by berry on 2020-01-25.
//


//
// ROUTER 到 DEALER 示例
//


#include <zmq.h>
#include <pthread.h>
#include "zhelpers.h"

#define NBR_WORKERS 10

static void *
worker_task (void *args)
{
    void *context = zmq_ctx_new();
    void *worker = zmq_socket(context, ZMQ_DEALER);

    s_set_id(worker);   // 设置一个可打印的身份
    zmq_connect(worker, "tcp://localhost:5671");

    int total = 0;
    while(1){
        // 告诉代理我们已经准备好执行工作
        s_sendmore(worker, "");
        s_send(worker, "Hi Boss");

        // 从代理获取工作负载，直到完成
        free(s_recv(worker));   // 封包分隔符
        char *workload = s_recv(worker);
        int finished = (strcmp(workload, "Fired!") == 0);
        free(workload);
        if (finished){
            printf("Completed: %d tasks\n", total);
            break;
        }
        total++;

        // 执行某些随机的工作
        s_sleep(randof(500) + 1);

    }
    zmq_close(worker);
    zmq_ctx_destroy(context);
    return NULL;

}


int main(void)
{
    void *context = zmq_ctx_new();
    void *broker = zmq_socket(context, ZMQ_ROUTER);

    zmq_bind(broker, "tcp://*:5671");
    srandom((unsigned) time(NULL));

    int worker_nbr;
    for (worker_nbr = 0; worker_nbr < NBR_WORKERS; worker_nbr++){
        pthread_t worker;
        pthread_create(&worker, NULL, worker_task, NULL);
    }
    // 运行 5 秒钟，然后告诉工人结束
    int64_t end_time = s_clock() + 5000;
    int workers_fired = 0;
    while(1){
        // 下一个消息给我们最近最少使用的工人
        char *identity = s_recv(broker);
        s_sendmore(broker, identity);
        free(identity);
        free(s_recv(broker));   // 封包分隔符
        free(s_recv(broker));   // 来自工人的响应
        s_sendmore(broker, "");

        // 鼓励工人，除非到了解雇他们的时候
        if (s_clock() < end_time)
            s_send(broker, "Work harder");
        else{
            s_send(broker, "Fired!");
            if (++workers_fired == NBR_WORKERS)
                break;
        }
    }
    zmq_close(broker);
    zmq_ctx_destroy(context);
    return 0;


}



