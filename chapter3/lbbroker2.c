//
// Created by berry on 2020-01-25.
//


//
// 负载均衡代理
// 演示 CZMQ API 的使用
//

#include <czmq.h>

#define NBR_CLIENTS 10
#define NBR_WORKERS 10
#define WORKER_READY "\001" // 工人已经准备就绪的信号


// 使用 REQ 套接字的基本请求 - 应答客户端
//

static void *
client_task(void *args)
{

    zctx_t *ctx = zctx_new();
    void *client = zsocket_new(ctx, ZMQ_REQ);
    zsocket_connect(client, "ipc://frontend.ipc");

    // 发送请求，获取应答
    while(true){
        zstr_send(client, "HELLO");
        char *reply = zstr_recv(client);
        if (!reply)
            break;
        printf("Client: %s\n", reply);
        free(reply);
        sleep(1);
    }
    zctx_destroy(&ctx);
    return NULL;
}


// 工人使用 REQ 套接字来做负载均衡
//
static void *
worker_task(void *args)
{
    zctx_t *ctx = zctx_new();
    void *worker = zsocket_new(ctx, ZMQ_REQ);
    zsocket_connect(worker, "ipc://backend.ipc");

    // 告诉代理我们已经准备好执行工作
    zframe_t *frame = zframe_new(WORKER_READY, 1);
    zframe_send(&frame, worker, 0);

    // 处理到达的消息
    while(true){
        zmsg_t *msg = zmsg_recv(worker);
        if (!msg)
            break;
        zframe_reset(zmsg_last(msg), "OK", 2);
        zmsg_send(&msg, worker);
    }
    zctx_destroy(&ctx);
    return NULL;
}


int main(void)
{
    zctx_t *ctx = zctx_new();
    void *frontend = zsocket_new(ctx, ZMQ_ROUTER);
    void *backend = zsocket_new(ctx, ZMQ_ROUTER);
    zsocket_bind(frontend, "ipc://frontend.ipc");
    zsocket_bind(backend, "ipc://backend.ipc");

    int client_nbr;
    for (client_nbr = 0; client_nbr < NBR_CLIENTS; client_nbr++)
        zthread_new(client_task, NULL);
    int worker_nbr;
    for (worker_nbr = 0; worker_nbr < NBR_WORKERS; worker_nbr++)
        zthread_new(worker_task, NULL);

    // 可用的工人队列
    zlist_t *workers = zlist_new();

    while(1){
        zmq_pollitem_t items [] = {
                {backend, 0, ZMQ_POLLIN, 0},
                {frontend, 0, ZMQ_POLLIN, 0},
        };
        // 只在我们有可用的工人时才轮训前端
        int rc = zmq_poll(items, zlist_size(workers)?2:1, -1);
        if (rc == -1)
            break;  //中断

        // 在后台处理工人活动
        if (items[0].revents & ZMQ_POLLIN){
            // 使用工人身份用于负载均衡
            zmsg_t *msg = zmsg_recv(backend);
            if (!msg)
                break;  // 中断
            zframe_t *identity = zmsg_unwrap(msg);
            zlist_append(workers, identity);

            // 转发消息给客户端，如果他不是一个 READY 消息
            zframe_t *frame = zmsg_first(msg);
            if (memcmp(zframe_data(frame), WORKER_READY, 1) == 0)
                zmsg_destroy(&msg);
            else
                zmsg_send(&msg, frontend);
        }
        if (items[1].revents & ZMQ_POLLIN){
            // 获取客户端请求，路由到第一个可用的工人
            zmsg_t *msg = zmsg_recv(frontend);
            if(msg){
                zmsg_wrap(msg, (zframe_t *)zlist_pop(workers));
                zmsg_send(&msg, backend);
            }
        }
    }
    // 当我们完成后，执行适当的清理
    while(zlist_size(workers)){
        zframe_t *frame = (zframe_t *) zlist_pop(workers);
        zframe_destroy(&frame);
    }
    zlist_destroy(&workers);
    zctx_destroy(&ctx);
    return 0;

}











