//
// Created by berry on 2020-01-24.
//


//
// 从多个套接字读取
// 此版本使用了一个简单的 recv 循环
//

#include <zmq.h>
#include "zhelpers.h"

int main(void)
{
    // 准备上下文和套接字
    void *context = zmq_ctx_new();

    // 连接到任务发生器
    void *receiver = zmq_socket(context, ZMQ_PULL);
    zmq_connect(receiver, "tcp://localhost:5557");

    // 连接到天气服务器
    void *subscriber = zmq_socket(context, ZMQ_SUB);
    zmq_connect(subscriber, "tcp://localhost:5556");
    zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, "10001", 6);

    // 处理来自两个套接字的消息
    // 对来自任务发生器的流量排优先级
    while(1){
        // 处理任何等待的任务
        int rc;
        for (rc = 0; !rc; ){
            zmq_msg_t task;
            zmq_msg_init(&task);
            if ((rc = zmq_msg_recv(&task, receiver, ZMQ_DONTWAIT)) != -1){
                // 处理任务
            }
            zmq_msg_close(&task);
        }
        // 处理任何等待的天气状况更新
        for (rc = 0; !rc; ){
            zmq_msg_t update;
            zmq_msg_init(&update);
            if((rc = zmq_msg_recv(&update, subscriber, ZMQ_DONTWAIT)) != -1){
                // 处理天气状况更新
            }
            zmq_msg_close(&update);
        }
        // 没有活动，所以休眠 1 秒
        s_sleep(1);
    }
    // 我们永远不会到达这里，但无论如何要做一些清理工作
    zmq_close(receiver);
    zmq_close(subscriber);
    zmq_ctx_destroy(context);
    return 0;

}


