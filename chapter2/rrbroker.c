//
// Created by berry on 2020-01-24.
//


#include <zmq.h>
#include <assert.h>
//#include "zhelpers.h"


int main (void)
{
    // 准备上下文和套接字
    void *context = zmq_ctx_new();
    void *frontend = zmq_socket(context, ZMQ_ROUTER);
    void *backend = zmq_socket(context, ZMQ_DEALER);
    int rc;
    rc = zmq_bind(frontend, "tcp://*:5559");
    assert(rc==0);
    rc = zmq_bind(backend, "tcp://*:5560");
    assert(rc==0);

    // 初始化轮询集合
    zmq_pollitem_t items [] = {
            {frontend, 0, ZMQ_POLLIN, 0},
            {backend, 0, ZMQ_POLLIN, 0}
    };
    // 在套接字之间切换消息
    while(1){
        zmq_msg_t message;
        int more = 0;   // 多部分检测

        zmq_poll(items, 2, -1);
        if (items[0].revents & ZMQ_POLLIN){
            while(1){
                // 处理消息的所有部分
                zmq_msg_init(&message);
                zmq_msg_recv(&message, frontend, 0);
                size_t more_size = sizeof(more);
                zmq_getsockopt(frontend, ZMQ_RCVMORE, &more, &more_size);
                zmq_msg_send(&message, backend, more?ZMQ_SNDMORE:0);
                zmq_msg_close(&message);
                if(!more)
                    break;  // 消息的最后部分
            }
        }
        if (items[1].revents & ZMQ_POLLIN){
            while(1){
                // 处理消息的所有部分
                zmq_msg_init(&message);
                zmq_msg_recv(&message, backend, 0);
                size_t more_size = sizeof(more);
                zmq_getsockopt(backend, ZMQ_RCVMORE, &more, &more_size);
                zmq_msg_send(&message, frontend, more?ZMQ_SNDMORE:0);
                zmq_msg_close(&message);
                if(!more)
                    break;  // 消息的最后部分
            }
        }

    }

    // 永远不会到达这里，但如果由于某种远影到达了，就执行清理工作
    zmq_close(frontend);
    zmq_close(backend);
    zmq_ctx_destroy(context);
    return 0;

}



