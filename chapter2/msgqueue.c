//
// Created by berry on 2020-01-24.
//

//
// 简单消息队列代理
// 与请求-应答代理相同，但使用 QUEUE 设备
//

#include <zmq.h>
#include <assert.h>

int main(void)
{
    void *context = zmq_ctx_new();

    // 面对客户端的套接字
    void *frontend = zmq_socket(context, ZMQ_ROUTER);
    int rc = zmq_bind(frontend, "tcp://*:5559");
    assert (rc == 0);

    // 面对服务的套接字
    void *backend = zmq_socket(context, ZMQ_DEALER);
    rc = zmq_bind(backend, "tcp://*:5560");
    assert(rc == 0);

    // 启动代理
    zmq_proxy(frontend, backend, NULL);

    // 我们永远不会到达这里......
    zmq_close(frontend);
    zmq_close(backend);
    zmq_ctx_destroy(context);
    return 0;


}





