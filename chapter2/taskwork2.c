//
// Created by berry on 2020-01-24.
//


//
// 任务工人 - 设计 2
// 添加发布 - 订阅流来接收和响应
//

#include <zmq.h>
#include <stdlib.h>
#include "zhelpers.h"

int main(void)
{
    void *context = zmq_ctx_new();

    // 用于接收消息的套接字
    void *receiver = zmq_socket(context, ZMQ_PULL);
    zmq_connect(receiver, "tcp://localhost:5557");

    // 消息发往的套接字
    void *sender = zmq_socket(context, ZMQ_PUSH);
    zmq_connect(sender, "tcp://localhost:5558");

    // 用于控制输入的套接字
    void *controller = zmq_socket(context, ZMQ_PUB);
    zmq_connect(controller, "tcp://localhost:5559");
    zmq_setsockopt(controller, ZMQ_SUBSCRIBE, "", 0);

    // 处理来自接受者和控制器的消息
    zmq_pollitem_t items [] = {
            {receiver, 0, ZMQ_POLLIN, 0},
            {controller, 0, ZMQ_POLLIN, 0},
    };

    // 处理来自两个套接字的消息
    while(1) {
        zmq_msg_t message;
        zmq_poll(items, 2, -1);
        if (items[0].revents & ZMQ_POLLIN){
            zmq_msg_init(&message);
            zmq_msg_recv(&message, receiver, 0);

            // 执行工作
            s_sleep(atoi((char *) zmq_msg_data(&message)));

            // 将结果发送到接收器
            zmq_msg_init(&message);
            zmq_msg_send(&message, sender, 0);

            // 用于查看器的简易过程指示器
            printf(".");
            fflush(stdout);

            zmq_msg_close(&message);
        }
        // 任何等待中的控制器命令相当于 "KILL"
        if (items[1].revents & ZMQ_POLLIN)
            break;  // 退出循环
    }
    // 完成
    zmq_close(receiver);
    zmq_close(sender);
    zmq_close(controller);
    return 0;

}






