//
// Created by berry on 2020-01-25.
//

//
// 发布 - 订阅封包发布者
// 注意 zhelpers.h 文件也提供了 s_sendmore
//

#include <zmq.h>
#include <unistd.h>
#include "zhelpers.h"


int main(void)
{
    // 准备上下文和发布者
    void *context = zmq_ctx_new();
    void *publisher = zmq_socket(context, ZMQ_PUB);
    zmq_bind(publisher, "tcp://*:5563");

    while(1){
        // 编写两个小小，每个消息都有带有一个封包和内容
        s_sendmore(publisher, "A");
        s_send(publisher, "We dont't want to see this");
        s_sendmore(publisher, "B");
        s_send(publisher, "We would like to see this");
        sleep(1);
    }
    // 我们永远不会到达这里，但如果因为任何原因到达了，就执行清理工作
    zmq_close(publisher);
    zmq_ctx_destroy(context);
    return 0;

}






