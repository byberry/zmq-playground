//
// Created by berry on 2020-01-25.
//

//
// 同步的发布者
//

#include <zmq.h>
#include <unistd.h>
#include "zhelpers.h"


// 我们等待 10 个订阅者
#define SUBSCRIBERS_EXPECTED 3

int main(void)
{
    void *context = zmq_ctx_new();

    // 与客户端交流的套接字
    void *publisher = zmq_socket(context, ZMQ_PUB);
    zmq_bind(publisher, "tcp://*:5561");

    // 用于接收信号的套接字
    void *syncservice = zmq_socket(context, ZMQ_REP);
    zmq_bind(syncservice, "tcp://*:5562");

    // 从订阅者获得同步
    printf("Waiting for subscribers\n");
    int subscribers = 0;
    while (subscribers < SUBSCRIBERS_EXPECTED) {
        // - 等待同步请求
        char *string = s_recv(syncservice);
        free(string);
        // - 发送同步应答
        s_send(syncservice, "");
        subscribers++;
    }

    // 现在广播正好 1M 个更新，后面跟着 END
    printf("Broadcasting messages\n");
    int update_nbr;
    for (update_nbr = 0; update_nbr < 1000000; update_nbr++)
        s_send(publisher, "Rhubarb");

    s_send(publisher, "END");
    printf("Message 'END' have been sent.");
    zmq_close(publisher);
    zmq_close(syncservice);
    zmq_ctx_destroy(context);
    return 0;




}





