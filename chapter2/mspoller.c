//
// Created by berry on 2020-01-24.
//

//
// 从多个套接字读取
// 此版本使用 zmq_poll()
//


#include <zmq.h>

int main(void)
{
    void *context = zmq_ctx_new();

    // 连接到任务发生器
    void *receiver = zmq_socket(context, ZMQ_PULL);
    zmq_connect(receiver, "tcp://localhost:5557");
    // 连接到天气服务器
    void *subscriber = zmq_socket(context, ZMQ_SUB);
    zmq_connect(subscriber, "tcp://localhost:5556");
    zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, "10001", 6);

    // 初始化轮训集
    zmq_pollitem_t items [] = {
            {receiver, 0, ZMQ_POLLIN, 0},
            {subscriber, 0, ZMQ_POLLIN, 0}
    };
    // 处理来自两个套接字的消息
    while(1){
        zmq_msg_t message;
        zmq_poll (items, 2, -1);

        if (items[0].revents & ZMQ_POLLIN) {
            zmq_msg_init(&message);
            zmq_msg_recv(&message, receiver, 0);
            // 处理任务
            zmq_msg_close(&message);
        }

        if (items[1].revents & ZMQ_POLLIN) {
            zmq_msg_init(&message);
            zmq_msg_recv(&message, subscriber, 0);
            // 处理天气状况更新
            zmq_msg_close(&message);
        }
    }
    zmq_close(receiver);
    zmq_close(subscriber);
    zmq_ctx_destroy(context);
    return 0;

}

