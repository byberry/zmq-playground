//
// Created by berry on 2020-01-24.
//


//
// 多线程 Hello World 服务器
//

#include <zmq.h>
#include <pthread.h>
#include <unistd.h>
#include "zhelpers.h"

static void *
worker_routine(void *context){
    // 与调度器交流的套接字
    void *receiver = zmq_socket(context, ZMQ_REP);
    zmq_connect(receiver, "inproc://workers");

    while(1){
        char *string = s_recv(receiver);
        printf("Received request: [%s]\n", string);
        free(string);
        // 执行某项"工作"
        sleep(1);
        // 将应答发回客户端
        s_send(receiver, "World");
    }
    zmq_close(receiver);
    return NULL;

}


int main(void)
{
    void *context = zmq_ctx_new();

    // 与客户端交流的套接字
    void *clients = zmq_socket(context, ZMQ_ROUTER);
    zmq_bind(clients, "tcp://*:5555");

    // 与工人交流的套接字
    void *workers = zmq_socket(context, ZMQ_DEALER);
    zmq_bind(workers, "inproc://workers");

    // 工人线程的投放池
    int thread_nbr;
    for (thread_nbr = 0; thread_nbr < 5; thread_nbr++){
        pthread_t worker;
        pthread_create(&worker, NULL, worker_routine, context);
    }

    // 通过一个排队代理将工作线程连接到客户端线程
    zmq_proxy(clients, workers, NULL);

    // 我们永远不会到达这里，不过如果因为任何原因到了这里，则执行清理
    zmq_close(clients);
    zmq_close(workers);
    zmq_ctx_destroy(context);
    return 0;





}







