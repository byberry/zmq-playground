//
// Created by berry on 2020-01-24.
//

//
// 天气代理设备
//

#include "zhelpers.h"

int main(void)
{
    void *context = zmq_ctx_new();

    // 天气服务器所在的位置
    void *frontend = zmq_socket(context, ZMQ_XSUB);
    zmq_connect(frontend, "tcp://192.168.55.210:5556");

    // 用于订阅者的公共端点
    void *backend = zmq_socket(context, ZMQ_XPUB);
    zmq_bind(backend, "tcp://10.1.1.0:8100");

    // 持续运行代理直到用户中断它为止
    zmq_proxy(frontend, backend, NULL);

    zmq_close(frontend);
    zmq_close(backend);
    zmq_ctx_destroy(context);
    return 0;


}



