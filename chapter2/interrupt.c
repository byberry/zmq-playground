//
// Created by berry on 2020-01-24.
//


//
// 显示如何处理 Ctrl-C (interrupt.c)
//


#include <zmq.h>
#include <stdio.h>
#include <signal.h>

// ------------------------------------------------
// 信号处理
//
// 在应用程序启动时调用 s_catch_signals(), 并且如果 s_interrupted 是 1，
// 则退出主循环。特别适合与 zmq_poll 一起使用


static int s_interrupted = 0;
static void s_signal_handler (int signal_value)
{
    s_interrupted = 1;
}

static void s_catch_signals (void)
{
    struct sigaction action;
    action.sa_handler = s_signal_handler;
    action.sa_flags = 0;
    sigemptyset(&action.sa_mask);
    sigaction(SIGINT, &action, NULL);
    sigaction(SIGTERM, &action, NULL);
}

int main(void)
{
    void *context = zmq_ctx_new();
    void *socket = zmq_socket(context, ZMQ_REP);
    zmq_bind(socket, "tcp://*:5555");

    s_catch_signals();
    while(1){
        // 阻塞读会在收到一个信号时退出
        zmq_msg_t message;
        zmq_msg_init(&message);
        zmq_msg_recv(&message, socket, 0);

        if (s_interrupted){
            printf("W: interrupt received, killing server...\n");
            break;
        }

    }

    zmq_close(socket);
    zmq_ctx_destroy(context);
    return 0;

}






