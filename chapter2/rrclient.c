//
// Created by berry on 2020-01-24.
//

//
// Hello World 客户端
// 连接 REQ 套接字到 tcp://localhost:5559
// 发送 "Hello" 到服务器，期待它返回 "World"
//

//#include <zmq.h>
#include "zhelpers.h"

int main(void)
{
    void *context = zmq_ctx_new();

    // 用来与服务器交流的套接字
    void *requester = zmq_socket(context, ZMQ_REQ);
    zmq_connect(requester, "tcp://localhost:5559");

    int request_nbr;
    for (request_nbr = 0; request_nbr != 10; request_nbr++){
        s_send(requester, "Hello");
        char *string = s_recv(requester);
        printf("Received reply %d [%s] \n", request_nbr, string);
        free(string);
    }
    zmq_close(requester);
    zmq_ctx_destroy(context);
    return 0;
}


