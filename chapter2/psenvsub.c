//
// Created by berry on 2020-01-25.
//


//
// 发布 - 订阅封包订阅者
//


#include <zmq.h>
#include "zhelpers.h"


int main(void)
{
    // 准备上下文和发布者
    void *context = zmq_ctx_new();
    void *subscriber = zmq_socket(context, ZMQ_SUB);
    zmq_connect(subscriber, "tcp://localhost:5563");
    zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, "B", 1);

    while(1){
        // 读取封包的地址
        char *address = s_recv(subscriber);
        // 读取消息内容
        char *contents = s_recv(subscriber);
        printf("[%s] %s\n", address, contents);
        free(address);
        free(contents);
    }
    // 我们永远不会到达这里，但如果因为任何原因到达了，就执行清理工作
    zmq_close(subscriber);
    zmq_ctx_destroy(context);
    return 0;


}





