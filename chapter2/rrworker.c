//
// Created by berry on 2020-01-24.
//

//
// Hello World 工人
// 连接 REP 套接字到 tcp://*:5560
// 期待从客户端发出的 "Hello", 用 "World" 来应答
//

#include <unistd.h>
#include "zhelpers.h"

int main(void)
{
    void *context = zmq_ctx_new();

    // 用来与客户端交流的套接字
    printf("before socket");
    void *responder = zmq_socket(context, ZMQ_REP);
    zmq_connect(responder, "tcp://localhost:5560");
    printf("after connect");
    while(1){
        // 等待来自客户端的下一个请求
        printf("Waiting...");
        void *string = s_recv(responder);
        printf("Received request: [%s]\n", string);
        free(string);

        // 执行一些工作
        sleep(1);

        // 将应答发回到客户端
        s_send(responder, "World");
    }
    // 我们永远不会到达这里，但如果由于某种原因到达了，就执行清理工作
    zmq_close(responder);
    zmq_ctx_destroy(context);
    return 0;

}


