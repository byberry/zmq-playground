//
// Created by berry on 2020-01-25.
//

//
// 同步的订阅者
//

#include <zmq.h>
#include <unistd.h>
#include "zhelpers.h"

int main(void)
{
    void *context = zmq_ctx_new();

    // 首先，连接订阅者套接字
    void *subscriber = zmq_socket(context, ZMQ_SUB);
    zmq_connect(subscriber, "tcp://localhost:5561");
    zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, "", 0);

    // 0MQ 太快了，所以我们要等一段时间
    sleep(1);

    // 其次，与发布者同步
    void *syncclient = zmq_socket(context, ZMQ_REQ);
    zmq_connect(syncclient, "tcp://localhost:5562");

    // - 发送同步请求
    s_send(syncclient, "");

    // - 等待同步应答
    char *string = s_recv(syncclient);
    free(string);

    // 第三，获得更新并报告收到的更新数量
    int update_nbr = 0;
    while(1){
        char *string = s_recv(subscriber);
        printf("%d\n", update_nbr);
        if (strcmp(string, "END") == 0){
            free(string);
            break;
        }
        free(string);
        update_nbr++;
    }
    printf("Received %d updates\n", update_nbr);

    zmq_close(subscriber);
    zmq_close(syncclient);
    zmq_ctx_destroy(context);
    return 0;

}




