//
// Created by berry on 2020-01-24.
//


//
// 任务接收器 - 设计 2
// 添加发布 - 订阅流来将 kill 信号发送给工人
//

#include <zmq.h>
#include <unistd.h>
#include "zhelpers.h"


int main(void)
{
    void *context = zmq_ctx_new();

    // 用于接收消息的套接字
    void *receiver = zmq_socket(context, ZMQ_PULL);
    zmq_bind(receiver, "tcp://*:5558");

    // 用于工人控制的套接字
    void *controller = zmq_socket(context, ZMQ_PUB);
    zmq_bind(controller, "tcp://*:5559");

    // 等待批处理的开始
    char *string = s_recv(receiver);
    free(string);

    // 启动时钟
    int64_t start_time = s_clock();

    // 处理 100 个确认
    int task_nbr;
    for (task_nbr = 0; task_nbr < 100; task_nbr++){
        char *string = s_recv(receiver);
        free(string);
        if ((task_nbr/10)*10 == task_nbr)
            printf(":");
        else
            printf(".");
        fflush(stdout);
    }
    printf("Total elapsed time: %d msec\n",
            (int) (s_clock() - start_time));

    // 发送 kill 信号给工人
    s_send(controller, "KILL");

    sleep(1); // 给 0MQ 时间来传递

    zmq_close(receiver);
    zmq_close(controller);
    zmq_ctx_destroy(context);
    return 0;




}






