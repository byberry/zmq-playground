//
// Created by berry on 2020-01-24.
//

//
// 多线程中继
//

#include <zmq.h>
#include <pthread.h>
#include "zhelpers.h"

static void *
step1(void *context){
    // 连接到 step2, 并告诉它我们已经准备就绪
    void *xmitter = zmq_socket(context, ZMQ_PAIR);
    zmq_connect(xmitter, "inproc://step2");
    printf("Step 1 ready, signaling step 2\n");
    s_send(xmitter, "READY");
    zmq_close(xmitter);
    return NULL;
}

static void *
step2 (void *context){
    // 在启动 step1 前绑定 inproc 套接字
    void *receiver = zmq_socket(context, ZMQ_PAIR);
    zmq_bind(receiver, "inproc://step2");
    pthread_t thread;
    pthread_create(&thread, NULL, step1, context);

    // 等待信号并传递它
    char *string = s_recv(receiver);
    free(string);
    zmq_close(receiver);

    // 连接到 step3, 并告诉它我们已经准备就绪
    void *xmitter = zmq_socket(context, ZMQ_PAIR);
    zmq_connect(xmitter, "inproc://step3");
    printf("Step 2 ready, signaling step 3\n");
    s_send(xmitter, "READY");
    zmq_close(xmitter);

    return NULL;
}

int main (void)
{
    void *context = zmq_ctx_new();

    // 在启动 step2 前绑定 inproc 套接字
    void *receiver = zmq_socket(context, ZMQ_PAIR);
    zmq_bind(receiver, "inproc://step3");
    pthread_t thread;
    pthread_create(&thread, NULL, step2, context);

    // 等待信号
    char *string = s_recv(receiver);
    free(string);
    zmq_close(receiver);

    printf("Test successful!\n");
    zmq_ctx_destroy(context);
    return 0;



}






