//
// Created by berry on 2020-01-23.
//

//
// 任务发生器
// 将 PUSH 套接字绑定到 tcp://localhost:5557
// 通过那个套接字发送批量任务给工人
//

#include <zmq.h>
#include <unistd.h>
#include "zhelpers.h"


int main(void)
{

    void *context = zmq_ctx_new();

    // 用于发送消息的套接字
    void *sender = zmq_socket(context, ZMQ_PUSH);
    zmq_bind(sender, "tcp://*:5557");

    // 用于发送批次开始消息的套接字
    void *sink = zmq_socket(context, ZMQ_PUSH);
    zmq_connect(sink, "tcp://localhost:5558");

    printf("Press Enter when the workers are ready:");
    getchar();
    printf("Sending tasks to workers...\n");

    // 第一个消息是 "0"
    s_send(sink, "0");

    // 初始化随机数发生器
    srandom((unsigned) time(NULL));

    // 发送 100 个任务
    int task_nbr;
    int total_msec = 0;  // 预期消耗的总毫秒数
    for (task_nbr = 0; task_nbr < 100; task_nbr++){
        int workload;
        // 从 1 到 100 毫秒的随机工作负载
        workload = randof(100) + 1;
        total_msec += workload;
        char string [10];
        sprintf(string, "%d", workload);
        s_send(sender, string);
    }

    printf("Total expected cost: %d msec\n", total_msec);
    sleep(1);

    zmq_close(sink);
    zmq_close(sender);
    zmq_ctx_destroy(context);
    return 0;

}








