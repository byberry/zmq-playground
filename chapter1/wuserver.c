//
// Created by berry on 2020-01-23.
//


//
// 天气更新服务器
// 将 PUB 套接字绑定到 tcp://*:5556
// 发布随机的天气更新
//

#include <zmq.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

#include "zhelpers.h"



int main(void) {
    // 准备上下文
    void *context = zmq_ctx_new();
    void *publisher = zmq_socket(context, ZMQ_PUB);

    int rc = zmq_bind(publisher, "tcp://*:5556");
    assert(rc == 0);
    rc = zmq_bind(publisher, "ipc://weather.ipc");
    assert(rc == 0);

    // 初始化随机数发生器
    srandom((unsigned) time(NULL));
    while(1) {
        // 获取数值
        int zipcode, temprature, relhumidity;
        zipcode = randof(100000);
        temprature = randof(215) - 80;
        relhumidity = randof(50) + 10;

        // 发送消息给所有订阅者
        char update[20];
        sprintf(update, "%05d %d %d", zipcode, temprature, relhumidity);
        s_send(publisher, update);
    }
    zmq_close(publisher);
    zmq_ctx_destroy(context);
    return 0;

}







