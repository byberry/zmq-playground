//
// Created by berry on 2020-01-24.
//

//
// 任务工人
// 将 PULL 套接字连接到 tcp://localhost:5557
// 通过那个套接字收集来自发生器的工作负载
// 将 PUSH 套接字连接到 tcp://localhost:5558
// 通过那个套接字发送结果给接收器
//

#include <zmq.h>
#include "zhelpers.h"

int main(void)
{
    void *context = zmq_ctx_new();

    // 用于接收消息的套接字
    void *receiver = zmq_socket(context, ZMQ_PULL);
    zmq_connect(receiver, "tcp://localhost:5557");

    // 用于发送消息的套接字
    void *sender = zmq_socket(context, ZMQ_PUSH);
    zmq_connect(sender, "tcp://localhost:5558");

    // 永远的处理任务
    while(1){
        char *string = s_recv(receiver);
        // 用于查看器的简易过程指示器
        fflush(stdout);
        printf("%s.", string);

        // 不做工作
        s_sleep(atoi(string));
        free(string);

        // 将结果发送给接收器
        s_send(sender, "");
    }
    zmq_close(receiver);
    zmq_close(sender);
    zmq_ctx_destroy(context);
    return 0;
}


