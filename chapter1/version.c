//
// Created by berry on 2020-01-23.
//

#include "zhelpers.h"

int main(void) {
    int major, minor, patch;
    zmq_version(&major, &minor, &patch);
    printf("Current 0MQ version is %d.%d.%d\n", major, minor, patch);

    return EXIT_SUCCESS;
}
