//
// Created by berry on 2020-01-24.
//

//
// 任务接收器
// 将 PULL 套接字绑定到 tcp://localhost:5558
// 通过那个套接字收集来自各个工人的结果
//

#include <zmq.h>
#include "zhelpers.h"

int main(void)
{
    // 准备上下文和套接字
    void *context = zmq_ctx_new();
    void *receiver = zmq_socket(context, ZMQ_PULL);
    zmq_bind(receiver, "tcp://*:5558");

    // 等待批次的开始
    char *string = s_recv(receiver);
    free(string);

    // 现在启动时钟
    int64_t  start_time = s_clock();

    // 处理 100 个确认
    int task_nbr;
    for (task_nbr = 0; task_nbr < 100; task_nbr++){
        char *string = s_recv(receiver);
        free(string);
        if((task_nbr/10)*10 == task_nbr)
            printf(":");
        else
            printf(".");
        fflush(stdout);
    }
    // 计算并报告批次的用时
    printf("Total elapsed time: %d msec\n",
           (int) (s_clock() - start_time));

    zmq_close(receiver);
    zmq_ctx_destroy(context);
    return 0;
}



