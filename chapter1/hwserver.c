#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <zmq.h>

int main() {

    void *context = zmq_ctx_new();

    // 与客户端交流的套接字
    void *responder = zmq_socket(context, ZMQ_REP);
    zmq_bind(responder, "tcp://*:5555");

    while(1){
        // 等待来自客户端的下一个请求
        zmq_msg_t request;
        zmq_msg_init(&request);
        zmq_msg_recv(&request, responder, 0);
        printf("Received Hello\n");
        zmq_msg_close(&request);

        // 做某项工作
        sleep(1);

        // 将应答发回给客户端

        zmq_msg_t reply;
        zmq_msg_init_size(&reply, 5);
        memcpy(zmq_msg_data(&reply), "World", 5);
        zmq_msg_send(&reply, responder, 0);
        zmq_msg_close(&reply);
    }

    // 我们永远不会到达这里，不过如果到了这里，这就是我们结束它的方法
    zmq_close(responder);
    zmq_ctx_destroy(context);
    return 0;
}