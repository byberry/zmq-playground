
/*
 * Hello World 客户端
 * 将 REQ 套接字连接到 tcp://localhost:5555
 * 发送 "Hello" 给服务器，期待传回 "World"
 */

#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(void){

    void *context = zmq_ctx_new();

    // 与服务器交流的套接字
    printf("Connecting to hello world server...\n");
    void *requester = zmq_socket(context, ZMQ_REQ);
    zmq_connect(requester, "tcp://localhost:5555");

    int request_nbr;
    for (request_nbr = 0; request_nbr != 10; request_nbr++){
        zmq_msg_t request;
        zmq_msg_init_size(&request, 5);
        memcpy(zmq_msg_data(&request), "Hello", 5);
        printf("Sending Hello %d...\n", request_nbr);
        zmq_msg_send(&request, requester, 0);
        zmq_msg_close(&request);

        zmq_msg_t reply;
        zmq_msg_init (&reply);
        zmq_msg_recv(&reply, requester, 0);
        printf("Received World %d\n", request_nbr);
        zmq_msg_close(&reply);

    }

    sleep(2);
    zmq_close(requester);
    zmq_ctx_destroy(context);
    return 0;
}





